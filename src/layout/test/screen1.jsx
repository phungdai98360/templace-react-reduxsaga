import Highcharts from "highcharts";
import React, { useEffect, useState } from "react";
import SimpleTable from '../SimpleTable/SimpleTable'
const Screen1 = (props) => {
  const [chartPieOption,setChartPieOption]=useState({
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie'
    },
    title: {
      text: `Biểu đồ tròn - ${new Date() }`
    },
    tooltip: {
      pointFormat: '{series.name}: <b>{point.y}</b>'
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        colors:['#25628F','#ffb300','#d149cc','#42060f','#388e3c'],
        dataLabels: {
          enabled: true,
          format: '<b>{point.name}</b>: {point.y}',
          connectorColor: 'silver'
        }
      }
    },
    series: [{
      name: 'Số lượng',
      data: [
        {name: 'Yêu cầu mới', y: 10},
        {name: 'Chờ duyệt', y: 20},
        {name: 'Mua hàng', y: 50},
        {name: 'Thanh toán', y: 10},
        {name: 'Hoàn thành', y: 10},
      ]
    }]
  })
  useEffect(()=>{
    Highcharts.chart('container',chartPieOption)
  },[])
  return (
  <div style={{ padding: 24, minHeight: 360 }}>
    <figure class="highcharts-figure">
        <div id="container"></div>
        <p class="highcharts-description">
          Chart showing stacked columns for comparing quantities. Stacked charts
          are often used to visualize data that accumulates to a sum. This chart
          is showing data labels for each individual section of the stack.
        </p>
    </figure>
    <SimpleTable/>
  </div>
  );
}

Screen1.propTypes = {};

export default Screen1;
