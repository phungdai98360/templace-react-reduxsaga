import React from 'react';
import PropTypes from 'prop-types';
import {Button, Col, DatePicker, Row, Select, Form, Table} from 'antd'
import FilterFormItem from "../../components/Filter/FilterFormItem";
import { SearchOutlined, ClearOutlined } from "@ant-design/icons";
const {Option} = Select
const SimpleTable = props => {
    const [form] = Form.useForm();
    let filterItems = [
        {
          name: "staff_id",
          placeholder: "Người tạo ",
          className: "mr-2 mb-2",
          render: (submitOnClear) => (
            <Select
              showSearch
              allowClear
              onChange={submitOnClear}
              style={{ width: "100%" }}
              placeholder="Người tạo "
              optionFilterProp="children"
              filterOption={(input, option) =>
                option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
              }
            >
              {[].map((staff, index) => (
                <Option value={staff.code_staff} key={index}>
                  {staff.firt_name + " " + staff.last_name}
                </Option>
              ))}
            </Select>
          ),
        },
        {
          name: "created_from",
          placeholder: "Ngày tạo từ",
          className: "mr-2 mb-2",
          render: (submitOnClear) => (
            <DatePicker
              onChange={submitOnClear}
              format="DD/MM/YYYY"
              placeholder="Ngày tạo từ"
              className="w-100"
              style={{ width: "100%" }}
            />
          ),
        },
        {
          name: "created_to",
          placeholder: "Ngày tạo đến",
          className: "mb-2",
          render: (submitOnClear) => (
            <DatePicker
              onChange={submitOnClear}
              format="DD/MM/YYYY"
              placeholder="Ngày tạo đến"
              className="w-100"
              style={{ width: "100%" }}
            />
          ),
        },
      ];
    const onFinish=(value)=>{
        console.log(value)
        alert(`form receive ${JSON.stringify(value)}`)
    }
    const dataSource = [
        {
          key: '1',
          name: 'Mike',
          age: 32,
          address: '10 Downing Street',
        },
        {
          key: '2',
          name: 'John',
          age: 42,
          address: '10 Downing Street',
        },
      ];
      
      const columns = [
        {
          title: 'Name',
          dataIndex: 'name',
          key: 'name',
        },
        {
          title: 'Age',
          dataIndex: 'age',
          key: 'age',
        },
        {
          title: 'Address',
          dataIndex: 'address',
          key: 'address',
        },
      ];
    return (
        <div>
            <Form
                name="dealer_filter_form"
                form={form}
                onFinish={onFinish}
                style={{ marginLeft: 10, marginRight: 10 }}
            >
                <FilterFormItem form={form} renderFilter={filterItems} />
                {/* </div> */}
                <Row>
                    <Col md={24} style={{ textAlign: "right" }}>
                    <div className="d-flex justify-content-end">
                        <Button
                        className="ml-2"
                        type="primary"
                        htmlType="submit"
                        icon={<SearchOutlined />}
                        >
                        Tìm
                        </Button>
                        <Button
                        className="ml-2"
                        icon={<ClearOutlined />}
                        >
                        Xóa
                        </Button>
                    </div>
                    </Col>
                </Row>
            </Form>
            <div className="mt-4">
            <Table dataSource={dataSource} columns={columns} />
            </div>
        </div>
    );
};

SimpleTable.propTypes = {
    
};

export default SimpleTable;