import React from "react";
import "./App.css";
import HeaderLayout from "./components/Header/index";
import SiderLayout from "./components/slider/index";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import { Layout } from "antd";
import Screen1 from "./layout/test/screen1";
function App() {
  return (
    <Router>
      <Layout>
        <SiderLayout />
        <Layout className="site-layout">
          <HeaderLayout />
          <Switch>
            <Route path="/">
              <Screen1 />
            </Route>
          </Switch>
        </Layout>
      </Layout>
    </Router>
  );
}

export default App;
